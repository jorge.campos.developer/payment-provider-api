package br.com.geradordedevs.paymentproviderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentProviderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentProviderApiApplication.class, args);
	}

}
